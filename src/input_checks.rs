use crate::{
    log,
    constants::{
        PAN_LENGTH, PAN_WITHOUT_VERIFICATION_DIGIT_LENGTH
    },
    VerificationDigitError
};

pub trait PanInput 
    where 
        Self: Sized
{
    fn validate(self)-> Result<Self, VerificationDigitError>;
}

pub trait PanWithoutVerificationDigitInput 
    where 
        Self: Sized
{
    fn validate(self)-> Result<Self, VerificationDigitError>;
}

impl PanInput for &[u32] {
    fn validate(self)-> Result<Self, VerificationDigitError> {
        log::trace!("Checking PAN length");
        check_length(PAN_LENGTH, self)
    }
}

#[cfg(test)]
mod pan_input_impl_test {
    use super::*;
    #[test]
    fn when_pan_length_is_ok(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7];
        let result = PanInput::validate(&v[..]);
        assert!(result.is_ok());
    }

    #[test]
    fn when_pan_length_is_small(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5,6];
        let result = PanInput::validate(&v[..]);
        assert!(result.is_err());
    }
    #[test]
    fn when_pan_length_is_big(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8];
        let result = PanInput::validate(&v[..]);
        assert!(result.is_err());
    }
}

impl PanWithoutVerificationDigitInput for &[u32] {
    fn validate(self)-> Result<Self, VerificationDigitError> {
        log::trace!("Checking first digits length");
        check_length(PAN_WITHOUT_VERIFICATION_DIGIT_LENGTH, self)
    }
}

#[cfg(test)]
mod pan_without_verification_digit_input_impl_test {
    use super::*;
    #[test]
    fn when_pan_length_is_ok(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5,6];
        let result = PanWithoutVerificationDigitInput::validate(&v[..]);
        assert!(result.is_ok());
    }

    #[test]
    fn when_pan_length_is_small(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5];
        let result = PanWithoutVerificationDigitInput::validate(&v[..]);
        assert!(result.is_err());
    }
    #[test]
    fn when_pan_length_is_big(){
        let v = vec![1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8];
        let result = PanWithoutVerificationDigitInput::validate(&v[..]);
        assert!(result.is_err());
    }
}

fn check_length(expected_length: u32, vec: &[u32])-> Result<&[u32], VerificationDigitError> {
    log::trace!("Verifing input length");
    let length = vec.len() as u32;
    if length == expected_length { 
        Ok(vec) 
    } else {
        log::error!("The length of the input is invalid.");
        Err( VerificationDigitError::IvalidLength(expected_length, length)) 
    }
}
