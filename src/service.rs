use crate::log;

pub fn validate_verification_digit(first_digits: &[u32], verification_digit: u32)-> bool {
    log::info!("Checking verification digit.");
    verification_digit == generate_verification_digit(first_digits)
}

pub fn split_pan(pan: &[u32])-> (&[u32], u32) {
    log::info!("Retrieving verification digit.");
    (&pan[0..15], pan[15])
}

pub fn generate_verification_digit(digits: &[u32])-> u32 {
    log::info!("Generating verification digit.");
    calculate_verification_digit(
        calculate_new_digits(digits))
}

fn calculate_new_digits(digits: &[u32])-> impl Iterator<Item = u32> + '_ {
    log::info!("Calculating new digits");
    digits.iter()
        .rev()
        .enumerate()
        .map(|(i,val)| {
            if i%2 == 0 {
                calculate_new_value(val)
            } else {
                *val}})
}

fn calculate_new_value(value: &u32)-> u32 {
    log::info!("Calculating new even position value");
    let new_value = value * 2;
    if new_value > 9 {
        log::trace!("New even value has more than one digit");
        log::trace!("Adding the new value digits");
        (new_value % 10) + (new_value / 10)
    } else {
        log::trace!("New even value has one digit");
        new_value
    }
}

fn calculate_verification_digit(digits: impl Iterator<Item = u32> )-> u32 {
    log::info!("Calculating verification digit");
    let total = digits.fold( 0, |ac, value| { ac + value });
    if total%10 == 0 {
        log::trace!("Verification digit is 0");
        0
    } else {
        log::trace!("Verification digit is not 0");
        (((total / 10) + 1) * 10) - total
    }
}

#[cfg(test)]
mod service_test {
    
    use super::*;

    const PAN: [u32; 16] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7,2];
    const INVALID_PAN: [u32; 15] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,8];
    const PAN_WITHOUT_VERIFICATION_DIGIT: [u32; 15] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7];
    const VERIFICATION_DIGIT: u32 = 2;

    #[test]
    fn when_verification_digit_is_generated() {
        assert_eq!(
            generate_verification_digit(&PAN_WITHOUT_VERIFICATION_DIGIT),
            VERIFICATION_DIGIT,
            "The verification digit should be the same"
        )
    }

    #[test]
    fn when_pan_is_verified() {
        assert!(
            validate_verification_digit(&PAN_WITHOUT_VERIFICATION_DIGIT, VERIFICATION_DIGIT),
            "The verification digit should be OK"
        )
    }
    #[test]
    fn when_pan_is_noy_verified() {
        assert!(
            !validate_verification_digit(&INVALID_PAN, VERIFICATION_DIGIT),
            "The verification digit should be not OK"
        )
    }

    #[test]
    fn when_pan_is_splited() {
        let (first, end) = split_pan(&PAN);
        assert_eq!(PAN_WITHOUT_VERIFICATION_DIGIT, first);
        assert_eq!(VERIFICATION_DIGIT, end);
    }
}
