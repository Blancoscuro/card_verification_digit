use crate::{
    log,
    functions, FirstDigitsInterface, VerificationDigitError, PanInterface
};

pub struct PanWithoutVerificationDigits<'s>
    where 
        Self: 's
{
    first_digits: &'s [u32]
}

impl <'s> PanWithoutVerificationDigits<'s> {

    pub fn new(digits: &'s [u32])-> Result<Self, VerificationDigitError> {
        log::debug!("Creating First digits DTO");
        FirstDigitsInterface::validate(digits)
            .map(|first_digits|{
                Self{first_digits}})
    }

    /// Creates a pan with the verification digit.
    pub fn generate_verification_pan(self)-> Pan<'s> {
        log::debug!("Generatirng new PAN from first digits.");
        Pan::new_with_first_digits(
            self.first_digits,
            functions::generate_verification_digit(self.first_digits)
        )
    }

}

#[cfg(test)]
mod pan_without_verification_digit_impl {
    use super::*;
    const PAN_WITHOUT_VERIFICATION_DIGIT: [u32; 15] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7];
    const INVALID_PAN_WITHOUT_VERIFICATION_DIGIT: [u32; 14] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7];
    const VERIFICATION_DIGIT: u32 = 2;
    
    #[test]
    fn when_a_dto_is_created() {
        let resutl = PanWithoutVerificationDigits::new(&PAN_WITHOUT_VERIFICATION_DIGIT);
        assert!(resutl.is_ok());
    }

    #[test]
    fn when_a_dto_is_not_created() {
        let resutl = PanWithoutVerificationDigits::new(&INVALID_PAN_WITHOUT_VERIFICATION_DIGIT);
        assert!(resutl.is_err());
    }

    #[test]
    fn when_a_pan_is_generated()-> Result<(),VerificationDigitError> {
        let resutl = PanWithoutVerificationDigits::new(&PAN_WITHOUT_VERIFICATION_DIGIT)?.generate_verification_pan();
        Ok (assert_eq!(VERIFICATION_DIGIT, resutl.verification_digit))
    }
}

pub struct Pan <'s>
    where 
        Self: 's
{
    first_digits: &'s [u32],
    pub verification_digit: u32
}

impl <'s> Pan<'s> {

    pub fn new(pan:&'s [u32])-> Result<Self, VerificationDigitError> {
        log::debug!("Creating PAN DTO.");
        PanInterface::validate(pan)
            .map(|input| {
                let (first_digits, verification_digit) = functions::split_pan(input);
                Self{first_digits, verification_digit}
            })
    }

    /// Validates that the verification digit in the pans is correct
    pub fn is_valid(&self)-> bool {
        log::debug!("Validating PAN");
        functions::validate_verification_digit(self.first_digits, self.verification_digit)
    }

    pub fn to_value(self)-> Vec<u32> {
        log::trace!("Retrieving PAN from DTO.");
        let mut pan = self.first_digits.to_vec();
        pan.push(self.verification_digit);
        pan
    }

    fn  new_with_first_digits(first_digits: &'s[u32], verification_digit: u32)-> Self {
        Self{first_digits, verification_digit}
    }

}

#[cfg(test)]
mod pan_impl {
    use super::*;

    const PAN: [u32; 16] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7,2];
    const BAD_PAN: [u32; 16] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7,9];
    const INVALID_PAN: [u32; 17] = [2,2,3,3,6,4,3,3,4,8,0,0,9,7,7,2,0];

    #[test]
    fn when_a_dto_is_created() {
        let result = Pan::new(&PAN);
        assert!(result.is_ok());
    }
    
    #[test]
    fn when_a_dto_is_not_created() {
        let result = Pan::new(&INVALID_PAN);
        assert!(result.is_err());
    }

    #[test]
    fn when_a_pan_is_valid()-> Result<(),VerificationDigitError> {
        let result = Pan::new(&PAN)?.is_valid();
        Ok(assert!(result))
    }

    #[test]
    fn when_a_pan_is_not_valid()-> Result<(),VerificationDigitError> {
        let result = Pan::new(&BAD_PAN)?.is_valid();
        Ok(assert!(!result))
    }

    #[test]
    fn when_a_pan_is_retrieved()-> Result<(),VerificationDigitError> {
        let result = Pan::new(&PAN)?.to_value();
        Ok(assert_eq!(&PAN, &result[..]))
    }
}
