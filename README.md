# Card Verification digit
This crate is for generating and verifing the verification digit of credit or debit cards.
## Usage
The crate exposes 2 DTOs:
- **PanWithoutVerificationDigitInput**: used for generating a PAN with the verification digit.
- **Pan**: used to verify the verification digit of an existing PAN
